function getBackend() {
  if (process.env.VUE_APP_BACKEND.indexOf("ws") === 0) {
    return process.env.VUE_APP_BACKEND;
  } else {
    const location = window.location;
    return `${location.protocol.replace("http", "ws")}//${location.host}${
      location.port ? ":" + location.port : ""
    }${process.env.VUE_APP_BACKEND}`;
  }
}

export default {
  state: {
    backend: getBackend()
  },
  getters: {
    backend(state) {
      return state.backend;
    }
  }
};

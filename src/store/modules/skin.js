export default {
  state: {
    skin: "day-skin"
  },
  mutations: {
    setSkin(state, skin) {
      state.skin = skin;
    }
  },
  getters: {
    skin(state) {
      return state.skin;
    }
  }
};

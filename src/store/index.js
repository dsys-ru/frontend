import Vue from "vue";
import Vuex from "vuex";
import createCache from "vuex-cache";

import backend from "./modules/backend";
import skin from "./modules/skin";

Vue.use(Vuex);

export default new Vuex.Store({
  state: {},
  mutations: {},
  actions: {},
  modules: { backend, skin },
  plugins: [createCache()]
});

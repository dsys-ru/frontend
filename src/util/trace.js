import moment from "moment";

export function parseTrace(trace) {
  const [poller, period, format] = trace.split("|");
  const [network, station, channel, seedname] = poller.trim().split(".");
  const [start, end] = period.trim().split(" - ");
  return {
    poller: { network, station, channel, seedname },
    period: {
      start: moment.utc(start),
      end: moment.utc(end)
    },
    format: format.trim()
  };
}

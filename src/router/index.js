import Vue from "vue";
import VueRouter from "vue-router";
import DefaultLayout from "@/layouts/default.vue";

Vue.use(VueRouter);

const routes = [
  {
    path: "/skins",
    name: "Skins",
    components: {
      layout: DefaultLayout,
      content: () => import(/* webpackChunkName: "test" */ "@/views/Skins.vue")
    }
  },
  {
    path: "/devices",
    alias: "/",
    name: "Devices",
    components: {
      layout: DefaultLayout,
      content: () =>
        import(/* webpackChunkName: "devices" */ "@/views/Devices.vue")
    }
  },
  {
    path: "/device/:host/:port",
    name: "Device",
    components: {
      layout: DefaultLayout,
      content: () =>
        import(/* webpackChunkName: "device" */ "@/views/Device.vue")
    },
    props: {
      content: true,
      layout: true
    }
  }
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes
});

export default router;

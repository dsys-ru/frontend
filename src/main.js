import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import store from "./store";
import "./styles/styles.scss";
import "@atlassian/aui";
import NotificationPlugin from "@/plugins/NotificationPlugin";
import BackendPlugin from "@/plugins/BackendPlugin";
import ConfirmDialogPlugin from "@/plugins/ConfirmDialogPlugin";
import "chart.js";

Vue.use(NotificationPlugin);
Vue.use(BackendPlugin);
Vue.use(ConfirmDialogPlugin);

Vue.config.productionTip = false;

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount("#app");

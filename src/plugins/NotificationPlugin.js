const NotificationPlugin = {
  install(Vue) {
    Vue.prototype.$notify = params => window.AJS.flag(params);
  }
};

export default NotificationPlugin;

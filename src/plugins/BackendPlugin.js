import store from "@/store";

const BackendPlugin = {
  install(Vue) {
    Vue.prototype.$backend = endpoint => {
      const backend = store.getters.backend;
      const vue = new Vue();

      return new Promise((resolve, reject) => {
        vue.socket = new WebSocket(`${backend}${endpoint ? endpoint : "/"}`);
        vue.socket.onopen = () => {
          resolve(vue);
        };
        vue.socket.onmessage = e => {
          const data = JSON.parse(e.data);
          vue.$emit("message", data);
        };
        vue.socket.onclose = () => {
          reject(
            new Error(
              "WebSocket could not be connected. Please make sure that backend server is started"
            )
          );
        };
        vue.sendMessage = msg => vue.socket.send(JSON.stringify(msg));
        vue.disconnect = () => vue.socket.close();
      });
    };
  }
};

export default BackendPlugin;

// Refer to: https://medium.com/@panzelva/writing-modals-for-vue-js-callable-from-anywhere-6994d180451
import $ from "jquery";
import ConfirmDialog from "../components/ConfirmDialog.vue";

const ConfirmDialogPlugin = {
  install(Vue) {
    this.EventBus = new Vue();
    Vue.component("confirm-dialog", ConfirmDialog);

    Vue.prototype.$confirmDialog = {
      show(params) {
        return new Promise(resolve => {
          ConfirmDialogPlugin.EventBus.$emit(
            "show",
            $.extend({}, params, { callback: resolve })
          );
        });
      }
    };
  }
};

export default ConfirmDialogPlugin;

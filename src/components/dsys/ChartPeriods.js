function decimate(trace, factor) {
  const data = [];
  let min = trace.data[0];
  let max = trace.data[0];
  let j = 0;

  for (let i = 1; i < trace.data.length; i++, j++) {
    if (j >= factor) {
      j = 0;
      data.push(min);
      data.push(max);
      min = trace.data[i];
      max = trace.data[i];
    } else {
      min = min > trace.data[i] ? trace.data[i] : min;
      max = max < trace.data[i] ? trace.data[i] : max;
    }
  }

  if (j != 0) {
    data.push(min);
    if (min !== max) data.push(max);
  }
  return { trace: trace.trace, data };
}

export default [
  {
    id: "M1",
    seconds: 60,
    title: "1 Minute",
    decimate: trace => trace
  },
  {
    id: "M5",
    seconds: 300,
    title: "5 Minutes",
    decimate: (trace, factor) =>
      decimate(trace, (5 * 60 * trace.data.length) / factor)
  },
  {
    id: "M15",
    seconds: 900,
    title: "15 Minutes",
    decimate: (trace, factor) =>
      decimate(trace, (15 * 60 * trace.data.length) / factor)
  },
  {
    id: "M30",
    seconds: 1800,
    title: "½ Hour",
    decimate: (trace, factor) =>
      decimate(trace, (30 * 60 * trace.data.length) / factor)
  },
  {
    id: "H1",
    seconds: 3600,
    title: "1 Hour",
    decimate: (trace, factor) =>
      decimate(trace, (60 * 60 * trace.data.length) / factor)
  }
];

# Apatit Server Frontend

To build you need `git`, `nodejs` and `yarn`:

```
git clone https://babinvn@bitbucket.org/dsys-ru/frontend.git
cd frontend
yarn install
yarn build
```

Frontend distribution is in directory `distr`

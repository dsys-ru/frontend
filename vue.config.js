const webpack = require("webpack");

module.exports = {
  publicPath: "/apt",
  css: {
    loaderOptions: {
      postcss: {
        warnings: false
      }
    }
  },
  configureWebpack: {
    plugins: [
      new webpack.ProvidePlugin({
        $: "jquery",
        jquery: "jquery",
        "window.jQuery": "jquery",
        jQuery: "jquery"
      })
    ]
  }
};
